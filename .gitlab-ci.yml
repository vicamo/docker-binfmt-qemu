include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'

variables:
  STAGING_IMAGE_PREFIX: "${CI_REGISTRY_IMAGE}/staging:${CI_PIPELINE_ID}-"
  IMAGE_PREFIX: "${CI_REGISTRY_IMAGE}/binfmt-qemu:"
  IMAGE_VERSION: "1.1"

stages:
  - prerequisites
  - build

updated Dockerfiles:
  stage: prerequisites
  image: "${CI_REGISTRY_IMAGE}/cached/bash:latest"
  script:
    - ./update.sh
    - test -z "$(git status -s)"

.build-template: &build-template
  stage: build
  image: "${CI_REGISTRY}/vicamo/docker-dind/cached/docker:git"
  services:
    - name: "${CI_REGISTRY}/vicamo/docker-dind/cached/docker:dind"
      alias: docker
  before_script:
    - env; set -x
    - echo "${CI_REGISTRY_PASSWORD}" |
        docker login -u "${CI_REGISTRY_USER}" --password-stdin "${CI_REGISTRY}"

    - apk update
    - apk add jq

    - i=0;
      image_prefixes="${IMAGE_PREFIX}";
      while true; do
        var=EXTRA_DOCKER_SERVER_${i};
        eval "server=\${${var}:-}";
        var=EXTRA_DOCKER_USER_${i};
        eval "user=\${${var}:-}";
        var=EXTRA_DOCKER_PASSFILE_${i};
        eval "passfile=\${${var}:-}";
        var=EXTRA_DOCKER_REPO_${i};
        eval "repo=\${${var}:-}";
        [ -n "${user}" ] && [ -n "${passfile}" ] && [ -n "${repo}" ] || break;

        cat "${passfile}" | base64 -d |
          docker login --username "${user}" --password-stdin
              ${server:+"${server}"};
        image_prefixes="${image_prefixes} ${repo}:";

        i=$(( i + 1 ));
      done

  script:
    - description=$(wget -q -O - "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}" | jq -r .description)
    - JOB_DISTRO="$(echo "${CI_JOB_NAME}" | cut -d ':' -f2)"
    - JOB_CODENAME="$(echo "${CI_JOB_NAME}" | cut -d ':' -f3)"
    - base_image=;
      case "${JOB_DISTRO}" in
      "debian") base_image="${CI_REGISTRY}/vicamo/docker-brew-debian/debian:${JOB_CODENAME}" ;;
      "ubuntu") base_image="${CI_REGISTRY}/vicamo/docker-brew-ubuntu-core/ubuntu:${JOB_CODENAME}" ;;
      esac;
    - staging_image="${STAGING_IMAGE_PREFIX}${JOB_CODENAME}"

    - docker build -t "${staging_image}"
          ${base_image:+--build-arg "BASE_IMAGE=${base_image}"}
          --label org.opencontainers.image.title="${CI_PROJECT_TITLE}"
          --label org.opencontainers.image.description="${description}"
          --label org.opencontainers.image.authors="You-Sheng Yang"
          --label org.opencontainers.image.url="${CI_PROJECT_URL}"
          --label org.opencontainers.image.documentation="${CI_PROJECT_URL}"
          --label org.opencontainers.image.created="$(date +%Y-%m-%dT%H:%M:%SZ)"
          --label org.opencontainers.image.source="${CI_PROJECT_URL}.git"
          --label org.opencontainers.image.version="${IMAGE_VERSION}.${CI_PIPELINE_IID}"
          --label org.opencontainers.image.revision="${CI_COMMIT_SHA}"
          --label org.opencontainers.image.licenses="Apache-2.0"
          "${JOB_DISTRO}/${JOB_CODENAME}";

    - lsmod || true
    - mount || true
    - ls -al /proc/sys/fs/binfmt_misc || true
    - docker run --rm --privileged "${staging_image}"
    - ls -al /proc/sys/fs/binfmt_misc/qemu-* || true

    - eval "$(wget -qO - http://archive.ubuntu.com/ubuntu/dists/devel/Release
        | awk '/^Codename:/ { print "devel_codename=" $2 } /^Version:/ { print "devel_version=" $2 }')"
    - devel_version_year="${devel_version%.*}"
    - devel_version_month="${devel_version#*.}"
    - devel_version_norm="$((devel_version_year * 12 + devel_version_month))"

    - for alias in oldstable stable testing unstable; do
        codename=$(wget -qO - "http://deb.debian.org/debian/dists/${alias}/Release"
          | awk '/^Codename:/ { print $2 }');
        eval "${alias}_codename=${codename}";
      done
    - aliases="${JOB_CODENAME}";
      for alias in oldstable stable testing unstable devel; do
        eval "alias_codename=\${${alias}_codename}";
        if [ "${JOB_CODENAME}" = "${alias_codename}" ]; then
          aliases="${aliases} ${alias}";
          if [ "${alias}" = "stable" ]; then
            aliases="${aliases} latest";
          fi;
        fi;
      done
    - if [ "${JOB_DISTRO}" = "ubuntu" ]; then
        eval "version=$({
          wget -qO - http://archive.ubuntu.com/ubuntu/dists/${JOB_CODENAME}/Release
            || wget -qO - http://old-releases.ubuntu.com/ubuntu/dists/${JOB_CODENAME}/Release; }
          | awk '/^Version:/ {print $2}')";
        aliases="${aliases} ${version}";

        version_year=${version%.*};
        version_month=${version#*.};
        version_norm=$((version_year * 12 + version_month));
        if [ "$((devel_version_norm - version_norm))" = "6" ]; then
          aliases="${aliases} rolling";
        fi;
      fi

    - echo "\#\#\#\#\# To push ${staging_image} as ${aliases} \#\#\#\#\#"
    - for alias in ${aliases}; do
        for prefix in ${image_prefixes}; do
          image="${prefix}${alias}";
          docker tag "${staging_image}" "${image}";
          docker tag "${staging_image}" "${image}-${IMAGE_VERSION}";
          if [ "${CI_COMMIT_BRANCH}" = "${CI_DEFAULT_BRANCH}" ]; then
            docker push --quiet "${image}";
            docker push --quiet "${image}-${IMAGE_VERSION}";
          fi;
        done;
      done

  after_script:
    - docker images

## BEGIN build JOBS ##

build:debian:bookworm:
  extends: .build-template

build:debian:bullseye:
  extends: .build-template

build:debian:buster:
  extends: .build-template

build:debian:sid:
  extends: .build-template

build:ubuntu:bionic:
  extends: .build-template

build:ubuntu:focal:
  extends: .build-template

build:ubuntu:jammy:
  extends: .build-template

build:ubuntu:kinetic:
  extends: .build-template

build:ubuntu:lunar:
  extends: .build-template

build:ubuntu:mantic:
  extends: .build-template

build:ubuntu:trusty:
  extends: .build-template

build:ubuntu:xenial:
  extends: .build-template

## END build JOBS ##

